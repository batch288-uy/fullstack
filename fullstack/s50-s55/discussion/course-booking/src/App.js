import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AppNavBar from './components/AppNavBar.js';
import Courses from './pages/Courses.js';
import Home from './pages/Home.js';
import Register from './pages/Register.js';
import Login from './pages/Login.js';
import Logout from './pages/Logout.js';
import NoPageFound from './pages/NoPageFound.js'
// The BrowserRouter component will enable us to simulate page navigation by synchronizing the shown content and the shown URL in the web browser.
// The Routes component holds all our Route components. It select which 'Route' component to show based on the url endpoint.
import {BrowserRouter, Route, Routes} from 'react-router-dom';
import {useState} from 'react';

// import UserProvider
import {UserProvider} from './UserContext.js';


function App() {

  const [user, setUser] = useState(localStorage.getItem('email'));
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const setLogin = () => {

    if(user === null){  

      setIsLoggedIn(false)

    } else {

      setIsLoggedIn(true)

    }

  }

  const unsetUser = () => {

    localStorage.clear()

  }

  return (

  <UserProvider value = {{user, setUser, unsetUser, isLoggedIn, setIsLoggedIn, setLogin}}>
    <BrowserRouter>
        <AppNavBar/>
        <Routes>
          <Route path = "/" element = {<Home/>} />

          <Route path = "/courses" element = {<Courses/>} />

          <Route path = "/register" element = {!isLoggedIn ? <NoPageFound/> : <Register/>} />

          <Route path = "/login" element = {!isLoggedIn ? <NoPageFound/> : <Login/>} />

          <Route path = "/logout" element = {<Logout/>} />

          <Route path = "*" element = {<NoPageFound/>} />
        </Routes>
    </BrowserRouter>
  </UserProvider>

   );
}

export default App;