import React from 'react';

//Create a context of object, a context object as the name states is a data that can be used to store information that can be shared to other components within the app.

//the context object is a different approach to passing information between components and allows easier access by avoiding the use of prop passing


//with the help iof createContext() method we were able to create a context stored in variable userContext
const UserContext = React.createContext();

//the provider components allows other components to consume or to use the context object and supply nessesary information needed tot he context object
export const UserProvider = UserContext.Provider;

export default UserContext;