import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';


//createRoot() assign the elemnt to be manged by react with its virtual  DOM
const root = ReactDOM.createRoot(document.getElementById('root'));


 //render() - method responsibsible for displaying the elemnt to be manged by React with its Virtual DOM

 root.render(
    <React.StrictMode>
      <App />
   </React.StrictMode>
  // <AppNavBar/>
 );

// const name = 'John Smith';
// const element = <h1>Hello, {name}! </h1>

// root.render(element);
