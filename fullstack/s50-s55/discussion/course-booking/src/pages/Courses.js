import coursesData from '../data/courses.js';
import CourseCard from '../components/CourseCard.js';

export default function Courses(){
	//we console the coursesData to check whether we imported the mock database properly
	// console.log(coursesData);

	//the prop that will be passed will be in object data and the name of the prop will be the name of the attribute when passed.

	const courses = coursesData.map(course => {

		return(
			<CourseCard key = {course.id} courseProp = {course}/>

			)
	})




	return(
	
		<>
			<h1 className = 'text-center mt-3'>Courses</h1>
			{courses}
		</>

		)
		
}