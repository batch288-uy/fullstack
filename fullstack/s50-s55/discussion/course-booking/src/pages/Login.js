import {Button, Col, Container, Form, Row} from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate, Link, useNavigate} from 'react-router-dom';
import UserContext from '../UserContext.js';



export default function Login(){

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isDisabled, setIsDisabled] = useState(true);
    // The getItem() method gets the value of the specified key from our local storage
    // const [user, setUser] = useState(localStorage.getItem('email'))
    const navigate = useNavigate();

    // we are going to consume or use the UserContext
    const {setUser, setLogin} = useContext(UserContext);

    useEffect(() => {

        if(email !== '' && password !== ''){

            setIsDisabled(false);

        } else {

            setIsDisabled(true);

        }
    }, [email, password])

    function login(e){

        e.preventDefault();
        alert("Login successful!")


        // Set the email of the authenticated user in the local storage
        // Syntax:
            // localStorage.setItem('propertyName', value)

        localStorage.setItem('email', email)

        setUser(localStorage.getItem('email'))

        navigate("/")

        setLogin()

        // setEmail('');
        // setPassword('');
    }


return(
        <Container className = "mt-5 mb-5">
            <Row>
                <Col className = "col-6 mx-auto">
                    <h1 className = "text-center">Login</h1>
                    <Form onSubmit = {e => login(e)}>
                         <Form.Group className="mb-3" controlId="formBasicEmail">
                           <Form.Label>Email address</Form.Label>
                           <Form.Control type="email" value = {email} onChange = {e => setEmail(e.target.value)} placeholder="Enter email" />
                         </Form.Group>

                         <Form.Group className="mb-3" controlId="formBasicPassword">
                           <Form.Label>Password</Form.Label>
                           <Form.Control type="password" value = {password} onChange = {e => setPassword(e.target.value)} placeholder="Password" />
                         </Form.Group>
                         <p>No account yet? <Link to = "/register">Sign up here</Link></p>
                         <Button variant="primary" type="submit" disabled = {isDisabled}>
                           Submit
                         </Button>
                       </Form>
                </Col>
            </Row>
        </Container>

    )
}