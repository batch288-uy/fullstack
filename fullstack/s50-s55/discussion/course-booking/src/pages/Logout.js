import {Navigate} from 'react-router-dom'
import UserContext from '../UserContext.js';
import {useContext, useEffect} from 'react';



export default function Logout(){


	// to clear content of our local storage we have to use the clear();

	const {unsetUser, setUser, setLogin} = useContext(UserContext)

	useEffect(() => {
		unsetUser();
		setUser(null);
		setLogin()
	})

	return(

		<Navigate to = "/login" />

		)
}