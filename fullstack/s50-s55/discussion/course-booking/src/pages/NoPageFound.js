import React from 'react';
import { Link } from 'react-router-dom';

export default function NoPageFound() {
  return (
    <div>
      <h3>Zuitt Booking</h3>
      <h1>Page Not Found</h1>
      <p>Go back to <Link to="/">homepage</Link></p>
    </div>
  );
}
