import {Container, Row, Col, Button, Form} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react'
import {Link, useNavigate, useLocation} from 'react-router-dom'
import UserContext from '../UserContext'

export default function Register () {

	const [email, setEmail] = useState('')
	const [password1, setPassword1] = useState('')
	const [password2, setPassword2] = useState('')

	const {setUser} = useContext(UserContext)

	const location = useLocation()
	const navigate = useNavigate()
	// useNavigate module is in react-router-dom, not in react, unlike useState and useEffect

	const [isDisabled, setIsDisabled] = useState(true)

	useEffect(() => {

		if(email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && password1.length > 6){
			setIsDisabled(false)
		}else{
			setIsDisabled(true)
		}

	}, [email, password1, password2])

	useEffect(() => {
	    const email = localStorage.getItem('email');
	    if ((location.pathname === '/login' || location.pathname === '/register') && email !== null) {
	      navigate('*');
	    }
	  }, [location.pathname, navigate]);

	function register (event) {
		event.preventDefault()

		localStorage.setItem('email', email)
		setUser(localStorage.getItem('email'))

		alert('Thank you for registering!')

		navigate('/login')

		
		setEmail('')
		setPassword1('')
		setPassword2('')
	}

	return (

		<Container>
			<Row>
				<Col  className = 'col-6 mx-auto'>
					<h1 className = 'mt-3 text-center'>Register Now</h1>
					<Form onSubmit = {event => register(event)}>
						<Form.Group className="mb-3" controlId="formBasicEmail">
							<Form.Label>Email address</Form.Label>
							<Form.Control 
								type="email" 
								value = {email}
								onChange = {event => setEmail(event.target.value)
								} 
								
								placeholder="Enter email"
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword1">
							<Form.Label>Password</Form.Label>
							<Form.Control 
								type="password" 
								value = {password1} 
								onChange = {event => setPassword1(event.target.value)}
								placeholder="Password"
							/>
						</Form.Group>

						<Form.Group className="mb-3" controlId="formBasicPassword2">
							<Form.Label>Confirm Password</Form.Label>
							<Form.Control 
								type="password" 
								value = {password2} 
								onChange = {event => setPassword2(event.target.value)}
								placeholder="Retype your nominated password"
							/>
						</Form.Group>

						<p>Already have an account? <Link to = '/login' style = {{textDecoration: 'none'}}>Login here</Link></p>

						<Button variant="primary" type="submit" disabled = {isDisabled}>
						Submit
						</Button>

					</Form>
				</Col>
			</Row>
		</Container>

	)
}